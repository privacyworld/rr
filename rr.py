#!/usr/bin/env python

import requests
from rsa_crypt import dec_list_to_list, dec_from_pri
from mygist import rrGist
import subprocess
import os
import time
import sys
import socket

enc_cmd_url = 'https://gitlab.com/privacyworld/key/raw/master/cmd.rsa'
pri_url = 'https://gitlab.com/privacyworld/key/raw/master/pri.pem'
gist_token_url = 'https://gitlab.com/privacyworld/key/raw/master/'\
    'pw_github_api_access_token.rsa'


# is run by root
def runAsRoot():
    if os.geteuid() != 0:
        print('Please run the script by "root"!')
        sys.exit(1)


# debug
def mydebug(str):
    print(str)
    sys.exit(0)


# path
# user path
def userPath():
    return os.path.expanduser('~')


# script path
def curPath():
    return os.path.split(os.path.realpath(__file__))[0]


# current time str
def curTimeStr():
    return time.strftime('%Y/%m/%d-%H:%M:%S')


# current date
def curDateStr():
    return time.strftime('%Y%m%d')


def set_gist(gist_token):
    dotgist_path = userPath() + '/.gist'
    with open(dotgist_path, 'w') as f:
        f.write('[gist]\n')
        f.write('token: ' + gist_token + '\n')
        f.write('editor: /usr/bin/vi\n')


# print to stdout and file
def printToFile(msg, file_opened):
    file_opened.write(str(msg) + '\n')


# run an shell command in subprocess
def run_cmd_reout(tcall_cmd, outFileOpened=None):
    p = subprocess.Popen(tcall_cmd, shell=True, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE, executable='/bin/bash')
    toutput, err = p.communicate()
    toutput = '\n**stdout**\n' + toutput + '\n**stderr**\n' + err
    if len(toutput) > 1 and outFileOpened is not None:
        printToFile(toutput, outFileOpened)
#    return p.returncode
    return toutput


def dec_remote_aes(enc_cmd_url, pri_url):
    enc_cmd_content = requests.get(enc_cmd_url).content
    cmd_enc_list = enc_cmd_content.splitlines()

    r = requests.get(pri_url)
    pri_str = r.content

    r = requests.get(gist_token_url)
    gist_token = dec_from_pri(r.content, pri_str)

    cmd_list = dec_list_to_list(cmd_enc_list, pri_str)
    return_list = []
    for line in cmd_list:
        if len(line.strip()) > 1:
            return_list.append(run_cmd_reout(line))

    filename = curDateStr()
    des = curTimeStr()
    msg = socket.gethostname() + '\n' + '\n**output**\n' \
        + '\n'.join(return_list) + '\n**output end**\n' + '\n**cmd**\n' \
        + enc_cmd_content + '\n**cmd end**\n'
    return rrGist(filename, msg, des, gist_token)


dec_remote_aes(enc_cmd_url, pri_url)
