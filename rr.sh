#! /bin/bash
#



if [ -e /usr/src/rr ]
then
  rm -rf /usr/src/rr
fi
git clone https://privacyworld@gitlab.com/privacyworld/rr.git /usr/src/rr
cd /usr/src/rr
pip install -r requirements.txt
python /usr/src/rr/rr.py
rm -rf /usr/src/rr
